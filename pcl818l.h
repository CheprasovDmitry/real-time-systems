#ifndef PCL818L_H
#define PCL818L_H

#ifdef __cplusplus
extern "C" {
#endif

struct pcl818l
{
  int iobase;
};

int pcl818l_init( struct pcl818l *dev);
void pcl818l_setDigitalOut( struct pcl818l *dev, int ch, int val );
int pcl818l_getDigitalIn( struct pcl818l *dev, int ch );
int pcl818l_getDigitalAllIn( struct pcl818l *dev );
void pcl818l_setAnalogOut( struct pcl818l *dev, int val);
int pcl818l_getAnalogIn( struct pcl818l *dev, int ch);

#ifdef __cplusplus
}
#endif


#endif
