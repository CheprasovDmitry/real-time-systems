#include <sys/io.h>
#include <fcntl.h>
#include <linux/errno.h>
#include "../pcl818l.h"
#include <pthread.h>
#include <rtai_lxrt.h>
#include <rtai_sem.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <termios.h>
#include <unistd.h>



/*
 * =========================================================
 * Список констант
 * =========================================================
 */
#define SLEEP_INTERVAL      100
#define SLEEP_TIME          SLEEP_INTERVAL * 1000000
#define a                   -0.593191437480759
#define b                   0.203404281259621
#define N                   0.35
#define Kp                  3.64
#define Ki                  0.01
#define Kd                  4.8
#define TcD                 0.14

const double dt = (float)SLEEP_INTERVAL / 1000.;
const double Ts = (float)SLEEP_INTERVAL / 1000.;
/*
 * =========================================================
 */


/*
 * =========================================================
 * Управление потоками
 * =========================================================
 */
static SEM *semBinary;

static int readThreadId;
static int calculateThreadId;

static RT_TASK  *inputTask;
static RT_TASK  *calculateTask;

RTIME ctask_period_count;
RTIME timer_period_count;
/*
 * =========================================================
 */


/*
 * =========================================================
 * Переменные для построения управления
 * =========================================================
 */
double integral = 0;
double prevErr = 0;
double prevDerivative = 0;

double uPrev = 0;
double uCur = 0;
double xPrev = 0;
/*
 * =========================================================
 */


/*
 * =========================================================
 * Переменные для чтения/вывода данных с/на устройства(-о)
 * =========================================================
 */
struct termios tio;
int tty_fd;

struct pcl818l pcl818l;
/*
 * =========================================================
 */


/*
 * =========================================================
 * Объявления функций
 * =========================================================
 */
// Working with COM Port.
void initCOMPort(char *[]);
void initAnalogOutput(void);
void taskInput(void *);
void output(double);

// Butterworth filter and PID functions block
double Saturation(double, double, double);
double PID(double);
double bFilter(const double, const double, const double);

void taskControlCalculate(void *);

// Internal functions
void configureTimer(void);
void writeNewData(const double);
int isDataReceived(void);
void cleanUp(void);
/*
 * =========================================================
 */

int main(int argc, char* argv[])
{
    rt_printk("Start of main\n");

    rt_task_init_schmod(nam2num("MAIN"), 0, 0, 0, SCHED_FIFO, 0xF);
    mlockall(MCL_CURRENT | MCL_FUTURE);

    initCOMPort(argv);

    initAnalogOutput();

    configureTimer();

    /* create binary semaphore with semaphore available and queue tasks on FIFO basis */
    semBinary=rt_typed_sem_init(nam2num("SEMBIN"), 1, BIN_SEM | FIFO_Q );

    readThreadId = rt_thread_create(taskInput, NULL, 10000);
    calculateThreadId = rt_thread_create(taskControlCalculate, NULL, 10000);

    rt_printk("TYPE <ENTER> TO TERMINATE\n");
    getchar();

    cleanup();

    rt_printk("Finished\n");
    return 0;
}

/*
 * Working with COM Port block.
 */
void initCOMPort(char* argv[])
{
    memset(&tio,0,sizeof(tio));

    tty_fd=open(argv[1], O_RDWR | O_NONBLOCK);

    rt_printk("tty_fd: %d\n", tty_fd);

    tcgetattr(tty_fd,&tio);

    tio.c_cflag =CS8|CREAD|CLOCAL;
    tio.c_cflag &= ~PARENB;
    tio.c_cflag &= ~CSTOPB;

    tio.c_lflag = 0;

    cfsetospeed(&tio,B38400);
    cfsetispeed(&tio,B38400);

    tcsetattr(tty_fd,TCSANOW,&tio);
}


void initAnalogOutput(void)
{
    pcl818l.iobase = 0x300;

    rt_printk("Start init\n");
    if (ioperm(pcl818l.iobase,0x20,1))
    {
        rt_printk("ioperm error");
    }

    int val = pcl818l_init( &pcl818l );

    rt_printk( "Init device PCL-818: %s\n", ( val == 0 ) ? "[ok]" : "[err]");

    if ( val != 0 )
    {
        rt_printk("init error");
    }
}


void taskInput(void *arg)
{
    inputTask = rt_task_init_schmod(nam2num("READ_DATA_TASK"), 0, 0, 0, SCHED_FIFO, 0xF);
    mlockall(MCL_CURRENT | MCL_FUTURE);
    rt_make_hard_real_time();

    char buf[50];
    int i = 0;
    unsigned char c='D';

    while (1)
    {
        int read_res = read(tty_fd, &c, 1);

        if (read_res < 0) continue;

        if (c != '\n' && i < 49)
        {
            buf[i++] = c;
        }
        else
        {
            buf[i] = 0;
            i = 0;

            while (buf[i++] != ' ');

            buf[i - 1] = 0;

            writeNewData(atoi(buf));

            i = 0;
        }
    }
}


void output(double fOutValue)
{
    //int intOut = fOutValue / 5 * 4096;
    int intOut = fOutValue / 2.32 * 2048;
    pcl818l_setAnalogOut( &pcl818l, intOut);
}


/*
 * Butterworth filter and PID functions block.
 */
double Saturation(double val, double lowerLimit, double upperLimit)
{
    if (val > upperLimit) return upperLimit;

    if (val < lowerLimit) return lowerLimit;

    return val;
}


double PID(double err)
{
    integral += prevErr * dt;

    double derivative = (2 * err - 2 * prevErr - (Ts - 2 * TcD) * prevDerivative) / (Ts + 2 * TcD);

    prevDerivative = derivative;
    prevErr = err;

    return Saturation(Kp * err + Ki * integral + Kd * derivative, -8, 8);
}


double bFilter(const double uPrev, const double uCur, const double xPrev)
{
    return b * uCur + b * uPrev - a * xPrev;
}


void taskControlCalculate(void *arg)
{
    calculateTask = rt_task_init_schmod(nam2num("CALCULATE_CONTROL_TASK"), 1, 0, 0, SCHED_FIFO, 0xF);
    mlockall(MCL_CURRENT | MCL_FUTURE);
    rt_make_hard_real_time();

    int retval = rt_task_make_periodic(calculateTask, rt_get_time() + ctask_period_count, ctask_period_count);

    if (retval != 0)
    {
        rt_printk("Error starting control calculating task.\n");
        return;
    }

    while (1)
    {
        if (!isDataReceived())
        {
            rt_task_wait_period();
            continue;
        }

        rt_sem_wait(semBinary);

        xPrev = bFilter(uPrev, uCur, xPrev);
        uPrev = uCur;

        double err = N - xPrev * (330.0/2000000.0);
        double outputData = Saturation(2.32 - PID(err), 0.5, 5);

        rt_printk("%f\t%f\n", xPrev, outputData);
        output(outputData);

        rt_sem_signal(semBinary);

        rt_task_wait_period();
    }
}

/*
 * Internal functions block.
 */
void configureTimer(void)
{
    rt_set_periodic_mode();

    ctask_period_count = nano2count(SLEEP_TIME);
    timer_period_count = start_rt_timer(ctask_period_count);
}

void writeNewData(const double uCr)
{
    rt_sem_wait(semBinary);

    uCur = uCr;

    rt_sem_signal(semBinary);
}

int isDataReceived(void)
{
    return uCur != 0 || uPrev != 0 || xPrev != 0;
}

void cleanup(void)
{
    pthread_cancel(readThreadId);
    pthread_cancel(calculateThreadId);

    stop_rt_timer();
    return;
}