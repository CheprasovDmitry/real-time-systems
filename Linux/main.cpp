#ifndef __APPLE__
    #include <sys/io.h>
#endif

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include "pcl818l.h"

#define SLEEP_INTERVAL      100
#define SLEEP_TIME          SLEEP_INTERVAL * 1000000
#define a                   -0.593191437480759
#define b                   0.203404281259621

//#define SLEEP_INTERVAL      200 * 1000000
//#define a                   -0.293407993026023
//#define b                   0.353296003486988

#define N                   0.4

#define Kp                  3.64
#define Ki                  0.01
#define Kd                  4.8
#define TcD                 0.14

const double dt = (float)SLEEP_INTERVAL / 1000.;
const double Ts = (float)SLEEP_INTERVAL / 1000.;

const struct timespec sleepTime = {0, SLEEP_TIME};

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

double integral = 0;
double prevErr = 0;
double prevDerivative = 0;

double uPrev = 0;
double uCur = 0;
double xPrev = 0;

struct termios tio;
int tty_fd;

struct pcl818l pcl818l;

// Working with COM Port.
void initCOMPort(char *[]);
void initAnalogOutput();
void output(double);


// Butterworth filter and PID functions block
double Saturation(double, double, double);
double PID(double);
double bFilter(const double, const double, const double);
static void *task(void *);


// Internal functions
void writeNewData(const double);
bool isDataReceived();


int main(int argc, char* argv[])
{
    initCOMPort(argv);
    
    initAnalogOutput();
    
    int rc;
    pthread_t thread;
    
    if((rc = pthread_create(&thread, NULL, &task, 0)))
    {
        printf("Thread creation failed: %d\n", rc);
    }
    
    char buf[50];
    int i = 0;
    unsigned char c='D';
    
    while (true)
    {
        int read_res = read(tty_fd, &c, 1);
        
        if (read_res < 0) continue;
        
        if (c != '\n' && i < 49)
        {
            buf[i++] = c;
        }
        else
        {
            buf[i] = 0;
            i = 0;
            
            while (buf[i++] != ' ');
            
            buf[i - 1] = 0;
            
            writeNewData(atoi(buf));
            
            i = 0;
        }
    }
    
    
    return 0;
}

/*
 * Working with COM Port block.
 */
void initCOMPort(char* argv[])
{
    memset(&tio,0,sizeof(tio));
    
    tty_fd=open(argv[1], O_RDWR | O_NONBLOCK);
    
    printf("tty_fd: %d\n", tty_fd);
    
    tcgetattr(tty_fd,&tio);
    
    tio.c_cflag =CS8|CREAD|CLOCAL;
    tio.c_cflag &= ~PARENB;
    tio.c_cflag &= ~CSTOPB;
    
    tio.c_lflag = 0;
    //tio.c_cc[VMIN]=1;
    //tio.c_cc[VTIME]=5;
    
    cfsetospeed(&tio,B38400);
    cfsetispeed(&tio,B38400);
    
    tcsetattr(tty_fd,TCSANOW,&tio);
}


void initAnalogOutput()
{
    pcl818l.iobase = 0x300;
    
    printf("Start init\n");
    if (ioperm(pcl818l.iobase,0x20,1))
    {
        printf("ioperm error");
    }
    
    int val = pcl818l_init( &pcl818l );
    printf( "Init device PCL-818: %s\n", ( val == 0 ) ? "[ok]" : "[err]");
    
    if ( val != 0 )
    {
        printf("init error");
    }
}


void output(double fOutValue)
{
    int intOut = fOutValue / 5 * 4096;
    pcl818l_setAnalogOut( &pcl818l, intOut);
}


/*
 * Butterworth filter and PID functions block.
 */
double Saturation(double val, double lowerLimit, double upperLimit)
{
    if (val > upperLimit) return upperLimit;
    
    if (val < lowerLimit) return lowerLimit;
    
    return val;
}


double PID(double err)
{
    integral += prevErr * dt;
    
    double derivative = (2 * err - 2 * prevErr - (Ts - 2 * TcD) * prevDerivative) / (Ts + 2 * TcD);
    
    prevDerivative = derivative;
    prevErr = err;
    
    return Saturation(Kp * err + Ki * integral + Kd * derivative, -8, 8);
}


double bFilter(const double uPrev, const double uCur, const double xPrev)
{
    return b * uCur + b * uPrev - a * xPrev;
}


static void *task(void *arg)
{
    while (true)
    {
        if (!isDataReceived())
        {
            nanosleep(&sleepTime, NULL);
            continue;
        }
        
        pthread_mutex_lock(&mutex);
        
        xPrev = bFilter(uPrev, uCur, xPrev);
        uPrev = uCur;
        
        double err = N - xPrev * (330.0/2000000.0);
        double outputData = Saturation(2.32 - PID(err), 0.5, 5);
        
        printf("%f\t%f\n", xPrev, outputData);
        output(outputData);
        
        pthread_mutex_unlock(&mutex);
        
        nanosleep(&sleepTime, NULL);
    }
}

/*
 * Internal functions block.
 */
void writeNewData(const double uCr)
{
    pthread_mutex_lock(&mutex);
    
    uCur = uCr;
    
    pthread_mutex_unlock(&mutex);
}


bool isDataReceived()
{
    return uCur != 0 || uPrev != 0 || xPrev != 0;
}