#ifdef __APPLE__
    #include <sys/uio.h>
#else
    #include <sys/io.h>
#endif

#include "pcl818l.h"
#include <unistd.h>
#include <stdio.h>

#define MAX_CHANNELS 16

#define PCL818_RANGE    0x01
#define PCL818_MUX        0x02
#define PCL818_STATUS   0x08
#define PCL818_CONTROL 0x09

static const unsigned char OneChannel[] = 
{ 
  0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
  0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff
};

static int pcl818_test(int iobase)
{
	outb(0x00,iobase + PCL818_MUX);
	usleep(1);
	if (inb(iobase + PCL818_MUX ) != 0x00)
		return -1;
	outb(0x55,iobase + PCL818_MUX);
	usleep(1);
	if (inb(iobase + PCL818_MUX) != 0x55)
		return -1;
	outb(0x00,iobase + PCL818_MUX);
	usleep(1);
	outb(0x18,iobase + PCL818_CONTROL);
	usleep(1);
	if (inb(iobase + PCL818_CONTROL) != 0x18)
		return -1;
	return 0;
}

int pcl818l_init( struct pcl818l *dev)
{
  int i;

  if ( pcl818_test( dev->iobase ) != 0 )
    return -1;

  //Config IRQ/DMA/Trigger
  outb(0x00, dev->iobase + PCL818_CONTROL );
 
  for( i = 0; i < MAX_CHANNELS; ++i)
  {
    //select chanel for init
    outb( OneChannel[i],dev->iobase + PCL818_MUX );
    // set range value ( +/- 5V ) 
    outb( 0x00,dev->iobase + PCL818_RANGE );
  }

  return 0;
}

void pcl818l_setDigitalOut( struct pcl818l *dev, int ch, int val )
{
  // val: 0 - clean, !0 - set
 
  val = ( val ) ? 1 : 0;
 
  if ( ch < 8 )
    outb(val << ch, dev->iobase + 0x03 );
 else 
   outb( val << ( ch - 8 ), dev->iobase + 0x0b );
}

int pcl818l_getDigitalIn( struct pcl818l *dev, int ch )
{
  int val; 
  
  if ( ch < 8 )
   return inb( dev->iobase + 0x03 ) & ( 0x01 << ch );
   
 return inb( dev->iobase + 0x0b ) & ( 0x01 << ( ch - 8 ) );
}

int pcl818l_getDigitalAllIn( struct pcl818l *dev )
{
  return inb( dev->iobase + 0x03 ) | (inb( dev->iobase + 0x0b ) << 0x08);
}

void pcl818l_setAnalogOut( struct pcl818l *dev, int val)
{
  outb( (val << 0x04 ) & 0xff, dev->iobase + 0x04 );
  outb( (val >> 0x04 ) & 0xff, dev->iobase + 0x05 );
}

int pcl818l_getAnalogIn( struct pcl818l *dev, int ch)
{
  int res;
// while( in8(dev->iobase + PCL818_STATUS) & 0x80 != 0x00 ) ;
  // select channel
  outb( OneChannel[ch & 0x0F],  dev->iobase + PCL818_MUX );

  // start conversion
  outb(0x00, dev->iobase + 0x00 );

  // wait set of channel
  //  while( in8(dev->iobase + PCL818_STATUS) & 0x80 != 0x00 )
  //     printf("%x = %d\n", in8(dev->iobase + PCL818_STATUS), ch);
    
  // clear INT (conversion end) flag
  outb(0x00, dev->iobase + PCL818_STATUS );

  // wait set of channel
  while( ( res = inb(dev->iobase + PCL818_STATUS) ) & 0x10 == 0x00 )
	;
 //     printf("%x = %d\n", res, ch);
 //   printf("%x = %d\n", res, ch);
  return ( inw( dev->iobase + 0x00 ) >> 0x04 ) & 0x0FFF;
}
